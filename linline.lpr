program linline;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes, SysUtils, CustApp
  { you can add units after this };

type

  { TLinLine }

  TLinLine = class(TCustomApplication)
  private
    FToWindows: Boolean;
    function GetSystemName: String;
  protected
    procedure DoRun; override;
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure WriteHelp; virtual;
    procedure Recode;
    property ToWindows: Boolean read FToWindows write FToWindows;
    property SystemName: String read GetSystemName;
  end;

{ TLinLine }

function TLinLine.GetSystemName: String;
begin
  if ToWindows then
    Result := 'Windows'
  else
    Result := 'Unix';
end;

procedure TLinLine.DoRun;
var
  ErrorMsg: String;
begin
  // quick check parameters
  ErrorMsg:=CheckOptions('hw', ['help', 'windows']);
  if ErrorMsg<>'' then begin
    ShowException(Exception.Create(ErrorMsg));
    Terminate;
    Exit;
  end;

  // parse parameters
  if HasOption('h', 'help') then begin
    WriteHelp;
    Terminate;
    Exit;
  end;

  { add your program here }
  if HasOption('w', 'windows') then
    FToWindows:=True
  else
    FToWindows:=False;

  Recode;

  // stop program loop
  Terminate;
end;

constructor TLinLine.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  StopOnException:=True;
end;

destructor TLinLine.Destroy;
begin
  inherited Destroy;
end;

procedure TLinLine.WriteHelp;
begin
  { add your help code here }
  writeln('Usage: ', ExeName, ' [-h] [-w] file');
  writeln('-h help');
  writeln('-w to Windows linebreaks');
end;

procedure TLinLine.Recode;
var
  FSIn, FSOut: TFileStream;
  SSIn, SSOut: TStringStream;
  NonOptions: TStringArray;
  F, I: Integer;
  FilePath: String;
  sFrom, sTo: String;
  C1, C2: Char;
  ReplacementCount: QWord;
  sSystemName: String;
begin
  NonOptions := GetNonOptions('hw', ['help', 'windows']);
  for F := 0 to High(NonOptions) do
  begin
    FilePath := NonOptions[F];
    try
      FSIn := TFileStream.Create(FilePath, fmOpenRead);
      SSIn := TStringStream.Create('');
      SSIn.CopyFrom(FSIn, FSIn.Size);
      sFrom := SSIn.DataString;
      sTo := '';
      ReplacementCount := 0;
      I := 1;
      while I <= Length(sFrom) do
      begin
        C1 := sFrom[I];
        if I + 1 <= Length(sFrom) then
          C2 := sFrom[I + 1]
        else
          C2 := #0;
        if ToWindows then
        begin
          case C1 of
          #$D:
            begin
              if C2 = #$A then
              begin
                sTo += #$D#$A;
                Inc(I);
              end;
            end;
          #$A:
            begin
              sTo += #$D#$A;
              Inc(ReplacementCount);
            end;
          else
            sTo += C1;
          end;
        end
        else // to Unix
        begin
          case C1 of
          #$D:
            begin
              Inc(ReplacementCount);
              // Leave #$D and continue with #$A in the next cycle
            end;
          else // including #$A
            sTo += C1;
          end;
        end;
        Inc(I);
      end;
    finally
      SSIn.Free;
      FSIn.Free;
    end;
    try
      FSOut := TFileStream.Create(FilePath, fmCreate);
      SSOut := TStringStream.Create(sTo);
      FSOut.CopyFrom(SSOut, SSOut.Size);
      sSystemName := SystemName;
      WriteLn('The file “', FilePath, '” was successfully converted to ',
        sSystemName, ' line breaks. ', ReplacementCount, ' replacements were ',
        'made.');
    finally
      SSOut.Free;
      FSOut.Free;
    end;
  end;
end;

var
  Application: TLinLine;
begin
  Application:=TLinLine.Create(nil);
  Application.Title:='LinLine';
  Application.Run;
  Application.Free;
end.

